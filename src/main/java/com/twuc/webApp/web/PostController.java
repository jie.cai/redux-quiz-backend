package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreatePostRequest;
import com.twuc.webApp.contracts.GetPostResponse;
import com.twuc.webApp.domain.Post;
import com.twuc.webApp.domain.PostRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/posts")
@CrossOrigin(origins = "*")
public class PostController {
    private final PostRepository postRepository;

    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GetMapping
    public ResponseEntity<List<GetPostResponse>> getAll() {
        List<GetPostResponse> posts = postRepository.findAll(
            Sort.by(Sort.Direction.ASC, "id")).stream()
            .map(p -> new GetPostResponse(p.getId(), p.getTitle(), p.getContent()))
            .collect(Collectors.toList());
        return ResponseEntity.ok(posts);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid CreatePostRequest request) {
        if (request == null) return ResponseEntity.badRequest().build();
        postRepository.save(new Post(request.getTitle(), request.getDescription()));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{postId}")
    public ResponseEntity delete(@PathVariable Long postId) {
        Optional<Post> post = postRepository.findById(postId);
        post.ifPresent(postRepository::delete);
        return ResponseEntity.ok().build();
    }
}
